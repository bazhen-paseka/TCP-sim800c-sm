
/* Includes -----------------------------------------------------------*/

	#include "TCP_sim800.h"
	#include "settings.h"

//********************************************************************************

/* Variables ---------------------------------------------------------*/

	char uart_buff_char[0xFF] = {0} ;
	volatile uint8_t flag = 0;
	int count = 9;
	char Unique_device_ID_char [30] = {0,} ;
	int connect_ok_i 			= 0 ;
	int connect_timeout_i 		= 0 ;
	int no_data_after_send_i	= 0 ;
	uint8_t at_status_u8		= 0 ;

//********************************************************************************

	extern UART_HandleTypeDef huart1;
	extern UART_HandleTypeDef huart2;
	extern TIM_HandleTypeDef  htim6 ;

//********************************************************************************

/* Private function prototypes -----------------------------------------------*/
	void clear_string(char *src);
	void chek_speed(void);
	void TCP_send() ;
//********************************************************************************

void TCP_sim800_init (void) {
	sprintf( uart_buff_char, "\r\n\r\n\t Start:\r\n"  ) ;
	HAL_UART_Transmit(UART_DEBUG, (uint8_t *)uart_buff_char, strlen(uart_buff_char), 1000 ) ;

	int soft_version_arr_int[3];
	soft_version_arr_int[0] = ((SOFT_VERSION) / 100) %10 ;
	soft_version_arr_int[1] = ((SOFT_VERSION) /  10) %10 ;
	soft_version_arr_int[2] = ((SOFT_VERSION)      ) %10 ;

	sprintf(uart_buff_char,"\t Ver: v%d.%d.%d\r\n" ,
			soft_version_arr_int[0] , soft_version_arr_int[1] , soft_version_arr_int[2] ) ;
	HAL_UART_Transmit( UART_DEBUG, (uint8_t *)uart_buff_char , strlen(uart_buff_char) , 1000 ) ;

	#define 	DATE_as_int_str 	(__DATE__)
	#define 	TIME_as_int_str 	(__TIME__)
	sprintf(uart_buff_char,"\t build: %s,  time: %s. \r\n" , DATE_as_int_str , TIME_as_int_str ) ;
	HAL_UART_Transmit( UART_DEBUG, (uint8_t *)uart_buff_char , strlen(uart_buff_char) , 1000 ) ;

	sprintf(uart_buff_char,"\t TCP + SIM800c + stm32-G070-RB \r\n"  ) ;
	HAL_UART_Transmit( UART_DEBUG, (uint8_t *)uart_buff_char , strlen(uart_buff_char) , 1000 ) ;

	sprintf( uart_buff_char, "\t for Debug: UART1(RC, column#2), 115200, 8, noPatiry, 1 \r\n" ) ;
	HAL_UART_Transmit(UART_DEBUG, (uint8_t *)uart_buff_char, strlen(uart_buff_char), 1000 ) ;

	//------------------------------------------------------------------------------------
		//	#define UID_BASE 0x1FFFF7E8 for stm32f103
		//	Unique device ID register (96 bits) for STM32G0b
		//	Base address: 0x1FFF 7590
		//	#define UID_BASE 0x1FFF7590	- #define не потрібен, бо це є системна інформація

		uint16_t *idBase0 = (uint16_t*)(UID_BASE);
		uint16_t *idBase1 = (uint16_t*)(UID_BASE + 0x02);
		uint32_t *idBase2 = (uint32_t*)(UID_BASE + 0x04);
		uint32_t *idBase3 = (uint32_t*)(UID_BASE + 0x08);

		sprintf(Unique_device_ID_char, "%04X-%04X-%08lX-%08lX", *idBase0, *idBase1, *idBase2, *idBase3);
		sprintf(uart_buff_char, "UID: %s\r\n", Unique_device_ID_char);
		HAL_UART_Transmit(UART_DEBUG, (uint8_t*)uart_buff_char, strlen(uart_buff_char), 1000);
	//------------------------------------------------------------------------------------

	HAL_GPIO_WritePin(USART2_MUX_GPIO_Port, USART2_MUX_Pin, 0);		//	select SIM800 (or BLE)

	sprintf( uart_buff_char, "GSM_Pin -> 1\r\n" ) ;
	HAL_UART_Transmit(UART_DEBUG, (uint8_t *)uart_buff_char, strlen(uart_buff_char), 1000 ) ;

    HAL_GPIO_WritePin(ON_OFF_GSM_GPIO_Port, ON_OFF_GSM_Pin, 1);
	HAL_Delay(1000);
  	HAL_GPIO_WritePin(ON_OFF_GSM_GPIO_Port, ON_OFF_GSM_Pin, 0);

//	sprintf( uart_buff_char, "GSM_Pin -> 0, wait... \r\n" ) ;
//	HAL_UART_Transmit(UART_DEBUG, (uint8_t *)uart_buff_char, strlen(uart_buff_char), 100);
//	HAL_Delay(3000);

  	for (int i = 9; i>0; i--) {
    	sprintf( uart_buff_char, "GSM_Pin -> 0, wait... %d\r", i ) ;
    	HAL_UART_Transmit(UART_DEBUG, (uint8_t *)uart_buff_char, strlen(uart_buff_char), 100);
    	HAL_GPIO_TogglePin (LED_GPIO_Port, LED_Pin );
  		HAL_Delay(1000);
  	}
	HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, RESET);

	HAL_UART_Transmit(UART_DEBUG, (uint8_t *)("\r\nUART_IT_ENABLE... "), strlen("\r\nUART_IT_ENABLE... "), 1000 ) ;
	__HAL_UART_ENABLE_IT( UART_DEBUG , UART_IT_RXNE);
	HAL_Delay(10);
	__HAL_UART_ENABLE_IT( UART_SIM800, UART_IT_RXNE);
	HAL_Delay(10);
	HAL_UART_Transmit(UART_DEBUG, (uint8_t *)(" Ok!\r\n"), strlen(" Ok!\r\n"), 100);

	do {
		chek_speed(); // скорость 115200
		sprintf( uart_buff_char, "AT_status: %d\r\n", at_status_u8 ) ;
		HAL_UART_Transmit(UART_DEBUG, (uint8_t *)uart_buff_char, strlen(uart_buff_char), 1000);
  		HAL_Delay(700);
	} while (at_status_u8 < 5) ;

	HAL_Delay(1000);
	set_comand(ATE);     // отключить «эхо»

	HAL_Delay(5000);
	set_comand(ATCPAS);  // проверка статуса модема


	HAL_Delay(5000);
	set_comand(ATCREG);  // проверка регистрации в сети - должен вернуть  +CREG: 0,1 краще 0,5

	//set_comand(ATCLIP1); // включить АОН
	//set_comand(ATS);     // поднимать трубку только "вручную"
	//set_comand(ATDDET);  // включить DTMF
	//set_comand(ATCCLKK); // установить дату/время
	set_comand(ATCMEE);  // включить расшифровку ошибок

	/*------------------- БЛОК INFA START --------------------*/
	//////////////////// различная инфа /////////////////////
	//set_comand(ATIPR);       // скорость usart'a модема
	//set_comand(ATI);         // название и версия модуля
	//set_comand(ATCGSN);      // считывание IMEI из EEPROM
	//set_comand(ATCSPN);      // оператор сети
	/*-------------------- БЛОК INFA END --------------------*/


	/*------------------- БЛОК TCP START --------------------*/
	//////////////////// TCP НАСТРОЙКА /////////////////////
	HAL_Delay(2000);
//	set_comand(ATCGATT1);         // включить GPRS сервис
//	HAL_Delay(5000);
//	set_comand(ATCGATT);          // проверить подключен ли к GPRS - +CGATT: 1
//	set_comand(ATCIPMUX0);        // установить режим одиночного соединения
//	chek_status(ATCIPSTATUS, 1);  // должно вернуть IP INITIAL
//	set_comand(ATCSTTAPN);        // APN, имя пользователя и пароль
//	chek_status(ATCIPSTATUS, 2);  // должно вернуть IP START
//	set_comand(ATCIICR);          // устанавливаем беспроводное подключение GPRS
//	chek_status(ATCIPSTATUS, 3);  // должно вернуть IP GPRSACT
//	set_comand(ATCIFSR);          // возвращает IP-адрес модуля
//	chek_status(ATCIPSTATUS, 4);  // должно вернуть IP STATUS
//	set_comand(ATCDNSCFG);        // установить сервера DNS

	set_comand( ATGSN  ) ;        		//  IMEI модуля
	set_comand( ATCSPN ) ;				//	operator
	set_comand( ATCOPS ) ;				// в какой сети зарегистрирован
	set_comand( ATCSQ  ) ;				//	информация о качестве сигнала

	/*-------------------- БЛОК TCP END --------------------*/

	//TCP_send(); // отправка данных серверу
	HAL_TIM_Base_Start_IT(&htim6);
	HAL_UART_Transmit(UART_DEBUG, (uint8_t *)("End Init\r\n"), strlen("End Init\r\n"), 100);
	//****************************************************************************************

	/*------------------- БЛОК SMS START --------------------*/
	/////////////////// настройки для работы с sms ////////////////
	sprintf( uart_buff_char, "--->> Start SMS\r\n" ) ;
	HAL_UART_Transmit(UART_DEBUG, (uint8_t *)uart_buff_char, strlen(uart_buff_char), 100);
	set_comand(ATCMGF);    // устанавливает текстовый режим смс-сообщения
	set_comand(ATCPBS);    // открывает доступ к данным телефонной книги SIM-карты
	set_comand(ATCSCS);    // кодировка текста - GSM
	set_comand(ATCNMI);    // настройка вывода смс в консоль

	set_comand(ATCSCA);		// Возвращает номер сервис центра отправки сообщений.

	sprintf( uart_buff_char, "--->> End SMS\r\n" ) ;
	HAL_UART_Transmit(UART_DEBUG, (uint8_t *)uart_buff_char, strlen(uart_buff_char), 100);
	/*-------------------- БЛОК SMS END --------------------*/

	set_comand(ATSMSMSG1);	//	номер отримувача СМС
	HAL_Delay(300);
	set_comand("Hello, yopta!\x1A");
	sprintf( uart_buff_char, "--->> Send SMS finish \r\n" ) ;
	HAL_UART_Transmit(UART_DEBUG, (uint8_t *)uart_buff_char, strlen(uart_buff_char), 100);
	//			> Hello, yopta!\x1A
	//			+CMGS: ХХ
	//			OK

	while(1) {
		HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
	  	HAL_Delay(500);
	}
}
//********************************************************************************

void TCP_sim800_main (void) {
	if(flag == 1) {	// срабатывает раз в секунду выводить время
		flag = 0;
		//get_date_time(); // будет раз в секунду выводить время
		HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin ) ;
		sprintf( uart_buff_char, " TCP_send pause: %02d\r" , count-- ) ;
		HAL_UART_Transmit(UART_DEBUG, (uint8_t *)uart_buff_char, strlen(uart_buff_char), 100);
	}

	if(count < 1) {
		count = 9;
		sprintf( uart_buff_char, "Good: %d; No_data_after: %d; Timeout: %d;\r\n" , 	 connect_ok_i, no_data_after_send_i, connect_timeout_i  ) ;
		HAL_UART_Transmit(UART_DEBUG, (uint8_t *)uart_buff_char, strlen(uart_buff_char), 100);

		HAL_UART_Transmit(UART_DEBUG, (uint8_t *)("\r\nNew_connect. "), strlen("\r\nNew_connect. "), 100);
		TCP_send();
	}

	if(gsm_available()) {	//если модуль что-то прислал
		uint16_t i = 0;

		char buf[GSM_AVAIL_SIZE] = {0,};
		char str[GSM_AVAIL_SIZE+7] = {0,};
		HAL_Delay(50);

		while(gsm_available())	{
			buf[i++] = gsm_read();
			if(i > GSM_AVAIL_SIZE - 1) break;
			HAL_Delay(1);
		}

		clear_string(buf); // очищаем строку от символов \r и \n  +PDP: DEACT

		if(strstr(buf, "RING") != NULL) { // ЕСЛИ ЭТО ЗВОНОК
			if(strstr(buf, "9823124561") != NULL) {	// если звонит нужный номер
				// что-то делаем
				HAL_UART_Transmit(UART_DEBUG, (uint8_t*)"My_number\n", strlen("My_number\n"), 1000);
				//incoming_call(); // можно принять звонок
				//HAL_NVIC_SystemReset(); // RESET
				disable_connection(); // сброс соединения
			}
		}
		else if(strstr(buf, "DEACT") != NULL) { // ЕСЛИ ЭТО +PDP: DEACT
			// что-то делаем
			HAL_UART_Transmit(UART_DEBUG, (uint8_t*)"My_DEACT\n", strlen("My_DEACT\n"), 1000);
			//HAL_NVIC_SystemReset(); // RESET
			set_comand(ATCIPSHUT); // разорвать все соединения и деактивировать интерфейс GPRS

			/////////// настройка как при старте, либо сделать ресет ///////////
			set_comand(ATCIPMUX0);        // установить режим одиночного соединения
			chek_status(ATCIPSTATUS, 1);
			set_comand(ATCSTTAPN);        // APN, имя пользователя и пароль
			chek_status(ATCIPSTATUS, 2);
			set_comand(ATCIICR);          // устанавливаем беспроводное подключение GPRS
			chek_status(ATCIPSTATUS, 3);
			set_comand(ATCIFSR);          // возвращает IP-адрес модуля
			chek_status(ATCIPSTATUS, 4);
			set_comand(ATCDNSCFG);       // установить сервера DNS
		}

		snprintf(str, GSM_AVAIL_SIZE+7, "We: %s.\r\n", buf);
		HAL_UART_Transmit(UART_DEBUG, (uint8_t*)str, strlen(str), 1000);
	}

	if(dbg_available()) {	//если послали в терминал какую-то команду, то она перенаправиться в модем
		uint8_t i = 0;
		char dbg_str[DBG_AVAIL_SIZE+3] = {0,};
		char dbg_buf[DBG_AVAIL_SIZE] = {0,};

		while(dbg_available())	{
			dbg_buf[i++] = dbg_read();
			if(i > DBG_AVAIL_SIZE - 1) break;
			HAL_Delay(1);
		}

		clear_string(dbg_buf);
		snprintf(dbg_str, DBG_AVAIL_SIZE+3, "%s\r\n", dbg_buf);
		HAL_UART_Transmit(UART_SIM800, (uint8_t*)dbg_str, strlen(dbg_str), 1000);
	}
}
//********************************************************************************


void clear_string(char *src)	{	// удалить символы \r и \n из строки
	char *dst = NULL;
	if(!src) return;
	uint8_t i = 0;

	for(dst = src; *src; src++)	{
		if (i < 2 && (*src == '\n' || *src == '\r'))		{
			i++;
			continue;
		} else if (*src == '\n' || *src == '\r') {
			*src = ' ';
		}
		*dst++ = *src;
	}
	*dst = 0;
}
//********************************************************************************

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)	{ // колбек таймера
	if(htim->Instance == TIM6) 	{
		flag = 1;
	}
}
//********************************************************************************

void chek_speed(void)	{
	char str[16] = {0,};
	HAL_UART_Transmit(UART_SIM800, (uint8_t*)"AT\r\n", strlen("AT\r\n"), 1000);
	HAL_Delay(300);

	if(gsm_available()) {	//если модуль что-то прислал
		uint16_t i = 0;

		while(gsm_available())	{
		  str[i++] = gsm_read();
		  if(i > 15) break;
		  HAL_Delay(2);
		}

		if(strstr(str, "OK") != NULL) {
		  //char buf[64] = {0,};
		  //int baudrate_i =  *UART_SIM800.Init.BaudRate ;
//		  snprintf(buf, 64, "AT-Ok; SIM800c modem speed was %d \r\n", baudrate_i );
//		  HAL_UART_Transmit(UART_DEBUG, (uint8_t*)buf, strlen(buf), 1000);
		  at_status_u8 ++;
		} else {
			HAL_UART_Transmit(UART_DEBUG, (uint8_t*)"No answer\r\n", strlen("No answer\r\n"), 1000);
			at_status_u8 = 0 ;
		}
	}
}
//********************************************************************************

void TCP_send() {
	char adres[] = "45.94.158.124";
	//char data[] = "GET /gsm.php?a=33&b=55";
	//char data[] = "GET /qwertyBazhen";
	char buf[GPRS_RX_BUFFER_SIZE] = {0,};
	uint16_t port = 8002;

	sprintf(buf, "TCP_send: ");
	HAL_UART_Transmit(UART_DEBUG, (uint8_t*)buf, strlen(buf), 1000);

	clear_gsm_buff();

	// создание подключения
	snprintf(buf, GPRS_RX_BUFFER_SIZE, "AT+CIPSTART=\"TCP\",\"%s\",\"%d\"\r\n", adres, port);
	HAL_UART_Transmit(UART_SIM800, (uint8_t*)buf, strlen(buf), 1000);
	HAL_UART_Transmit(UART_DEBUG, (uint8_t*)buf, strlen(buf), 1000);
	// ожидание "CONNECT OK" после AT+CIPSTART
	memset(buf, 0, GPRS_RX_BUFFER_SIZE);
	uint16_t timeout = 10000;

	for(uint16_t i = 0; i < timeout; i++) {// 20 sek
		if (i%1000 == 0) {
			sprintf(buf, "Answer timeout: %02d\r", (timeout-i)/1000);
			HAL_UART_Transmit(UART_DEBUG, (uint8_t*)buf, strlen(buf), 1000);
		}
		if(gsm_available())	{
			uint8_t k = 0;

			while(gsm_available()) {
				buf[k++] = gsm_read();
				if(k > GPRS_RX_BUFFER_SIZE - 1) break;
				HAL_Delay(1);
			}

			if(strstr(buf, "CONNECT OK") != NULL) {
				connect_ok_i++;
				HAL_UART_Transmit(UART_DEBUG, (uint8_t*)" Connect Ok\r\n", strlen(" Connect Ok\r\n"), 100); // debug
				break;
			}

			if(strstr(buf, "CONNECT FAIL") != NULL)	{
				HAL_UART_Transmit(UART_DEBUG, (uint8_t*)" Connect FAIL\r\n", strlen(" Connect FAIL\r\n"), 100); // debug
				// как-то обрабатываем ошибку
				return;
			}

			if(strstr(buf, "ALREADY CONNECT") != NULL) {
				HAL_UART_Transmit(UART_DEBUG, (uint8_t*)" ALREADY Connect\r\n", strlen(" ALREADY Connect\r\n"), 100); // debug
				HAL_UART_Transmit(UART_SIM800, (uint8_t*)"AT+CIPCLOSE\r\n", 13, 1000); // закрыть соединение, если оно не было закрыто при прошлом запросе
				return;
			}
		}

		if(i == timeout - 1) {
			connect_timeout_i++;
			HAL_UART_Transmit(UART_DEBUG, (uint8_t*)"Error, not Connect timeout\r\n", strlen("Error, not Connect timeout\r\n"), 1000);
			// как-то обрабатываем ошибку
			return;
		}
		HAL_Delay(1);
	}

	// AT+CIPSEND подготовка отправки данных, должно прилететь '>'
	HAL_Delay(100);
	HAL_UART_Transmit(UART_SIM800, (uint8_t*)"AT+CIPSEND\r\n", 12, 1000);
	HAL_UART_Transmit( UART_DEBUG, (uint8_t*)"AT+CIPSEND\r\n", 12, 1000);
	HAL_UART_Transmit (UART_DEBUG, (uint8_t*)"We wait on '>'... ", strlen("We wait on '>'... "), 1000); // debug
	// ожидание '>'
	memset(buf, 0, GPRS_RX_BUFFER_SIZE);
	timeout = 2000;

	for(uint16_t i = 0; i < timeout; i++) { // 2 sek
		if(gsm_available()) {
			uint8_t k = 0;

			while(gsm_available()) {
				buf[k++] = gsm_read();
				if(k > GPRS_RX_BUFFER_SIZE - 1) break;
				HAL_Delay(1);
			}

			if(strstr(buf, ">") != NULL) {
				HAL_UART_Transmit(UART_DEBUG, (uint8_t*)" - is present.\r\nWe send: ", strlen(" - is present.\r\nWe send: "), 1000); // debug
				break;
			}
		}

		if(i == timeout - 1) {
			HAL_UART_Transmit(UART_DEBUG, (uint8_t*)"Error, not '>'\r\n", strlen("Error, not '>'\r\n"), 1000);
			HAL_UART_Transmit(UART_SIM800, (uint8_t*)"AT+CIPCLOSE\r\n", 13, 1000); // закрыть соединение
			// как-то обрабатываем ошибку
			return;
		}
		HAL_Delay(1);
	}

	// отправка данных
	clear_gsm_buff();
	HAL_Delay(100);
	memset(buf, 0, GPRS_RX_BUFFER_SIZE);
	//snprintf(buf, GPRS_RX_BUFFER_SIZE, "%s HTTP/1.1\r\nHost: %s\r\nConnection: keep-alive\r\n\r\n%c", data, adres, (char)26);
	//snprintf(buf, GPRS_RX_BUFFER_SIZE, "%s HTTP/1.1\r\nHost: %s\r\nConnection: keep-alive\r\n\r\n%c", Unique_device_ID_char, adres, (char)26);
	snprintf(buf, GPRS_RX_BUFFER_SIZE, "UID:%s%c", Unique_device_ID_char,  (char)26);
	//snprintf(buf, 34, "UID:%s\r\n", Unique_device_ID_char );
	HAL_UART_Transmit(UART_DEBUG,  (uint8_t*)buf, sizeof(buf), 1000);
	HAL_UART_Transmit(UART_SIM800, (uint8_t*)buf, sizeof(buf), 1000);

	// ожидание 'SEND OK' и чтение ответа сервера
	memset(buf, 0, GPRS_RX_BUFFER_SIZE);
	timeout = 10000;

	HAL_UART_Transmit(UART_DEBUG, (uint8_t*)"\r\nWe wait on \"SEND OK\"... ", strlen("\r\nWe wait on \"SEND OK\"... "), 100);

	for(uint16_t i = 0; i < timeout; i++) {	// 10 sek
		if(gsm_available()) {
			uint16_t k = 0;
			while(gsm_available()) {
				buf[k++] = gsm_read();
				if(k > 11) break;
				HAL_Delay(1);
			}

			char *p = NULL;
			if((p = strstr(buf, "SEND OK")) != NULL) {
				for(uint16_t x = 0; x < 10000; x++) {	// 10 sek
					if(gsm_available()) {
						while(gsm_available()) {
							buf[k++] = gsm_read();
							if(k > GPRS_RX_BUFFER_SIZE - 1) break;
							HAL_Delay(1);
						}
						HAL_UART_Transmit(UART_DEBUG, (uint8_t*)" - is present.\r\n Answer: ", strlen(" - is present.\r\n Answer: "), 1000 ) ;
						HAL_UART_Transmit(UART_DEBUG, (uint8_t*)p + 9, strlen(p + 9), 1000);
						// тут можно парсить ответ
						HAL_UART_Transmit(UART_SIM800, (uint8_t*)"AT+CIPCLOSE\r\n", 13, 1000); // закрыть соединение
						HAL_UART_Transmit(UART_DEBUG, (uint8_t*)"<<<End answer.\r\n", strlen("<<<End answer.\r\n"), 1000 ) ;
						return;
					}

					if(x == 9999) {
						no_data_after_send_i++;
						HAL_UART_Transmit(UART_DEBUG, (uint8_t*)"No data after SEND OK\r\n", strlen("No data after SEND OK\r\n"), 1000);
						HAL_UART_Transmit(UART_SIM800, (uint8_t*)"AT+CIPCLOSE\r\n", 13, 1000); // закрыть соединение
						return;
					}
					HAL_Delay(1);
				}
			} else {
				HAL_UART_Transmit(UART_DEBUG, (uint8_t*)"Error, not SEND OK 1\r\n", strlen("Error, not SEND OK 1\r\n"), 1000);
				HAL_UART_Transmit(UART_SIM800, (uint8_t*)"AT+CIPCLOSE\r\n", 13, 1000); // закрыть соединение
				return;
			}
		}

		if(i == timeout - 1) {
			HAL_UART_Transmit(UART_DEBUG, (uint8_t*)"Error, not SEND OK 2\r\n", strlen("Error, not SEND OK 2\r\n"), 1000);
			HAL_UART_Transmit(UART_SIM800, (uint8_t*)"AT+CIPCLOSE\r\n", 13, 1000); // закрыть соединение
			return;
		}
		HAL_Delay(1);
	}
}
//********************************************************************************
