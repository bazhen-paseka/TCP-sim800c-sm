//********************************************************************************
#ifndef TCP_SIM800_INCLUDED_H_
#define TCP_SIM800_INCLUDED_H_
//********************************************************************************

/* Includes -----------------------------------------------------------*/
	#include "main.h"
 	#include "stdio.h"
 	#include <string.h>
	#include "gprs.h"
	#include "usart_ring.h"

//********************************************************************************
	
	#define GSM_AVAIL_SIZE 128
	#define DBG_AVAIL_SIZE 128

/* Function prototypes -----------------------------------------------*/
	void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);

	void TCP_sim800_init (void);
	void TCP_sim800_main (void);

//********************************************************************************


#endif /* TCP_SIM800_INCLUDED_H_ */
